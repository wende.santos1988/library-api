package com.start.testes.kt.library.utils

import com.start.testes.kt.library.domain.model.Book

class BookUtils {

    fun createNewBook(): Book
    {
        return Book(title = "Meu Livro", author = "author", isbn = "123")
    }

    fun createNewBookWithId(): Book {
        return Book( id = 1L, title = "Meu Livro", author = "author", isbn = "123")
    }

    fun createBookWithIdToUpdate(): Book {
        return Book(id = 1L, title = "some title", author = "same author", isbn = "123")
    }

}